(ns ui.routes
  (:require
    [re-frame.core :as re]
    [reitit.frontend :refer [router]]
    [reitit.frontend.easy :as rfe]
    [ui.events :as events]))

(defn game-board-start-controller
  [parameters]
  (re/dispatch [::events/ensure-game (get-in parameters [:path :game-uuid])]))

(defn clear-game-controller
  [_parameters]
  (re/dispatch [::events/clear-game]))

(def routes
  ["/"
   ["" {:name        ::home
        :controllers [{:start clear-game-controller}]}]
   ["logout" {:name        ::logout
              :controllers [{:start #(re/dispatch [::events/logout])}]}]
   ["new-game" {:name        ::new-game
                :controllers [{:start #(re/dispatch [::events/new-game])}]}]
   ["game/:game-uuid" {:name        ::game-board
                       :controllers [{:parameters {:path [:game-uuid]}
                                      :start      game-board-start-controller
                                      :stop       clear-game-controller}]}]
   ["past-games" {:name        ::past-games
                  :controllers [{:start #(re/dispatch [::events/user-games])}]}]])

(defn on-navigate
  [new-match]
  (when new-match
    (re/dispatch [::events/on-navigate new-match])))

(defn start!
  []
  (rfe/start!
    (router routes)
    on-navigate
    {:use-fragment true}))

(defmulti current-route-panel
  (fn [route-info] (get-in route-info [:data :name])))

(defmethod current-route-panel :default
  [route-info]
  [:div.text-bold "Invalid route - " route-info])

(ns ui.events
  (:require
    [ajax.core :as ajax]
    [day8.re-frame.http-fx]
    [day8.re-frame.async-flow-fx]
    [re-frame.core :refer [reg-event-db reg-event-fx reg-fx]]
    [reitit.frontend.controllers :refer [apply-controllers]]
    [ui.utils :as utils]))

(def json-response-format
  (ajax/json-response-format {:keywords? true}))

(reg-fx
  :set-route
  (fn [{:keys [url]}]
    (utils/set-hash! url)))

(reg-event-fx
  ::initialize-app
  (fn [_ _]
    {:db {}
     :fx [[:dispatch [::login]]]}))

(reg-event-db
  ::on-navigate
  (fn [db [_ new-match]]
    (let [old-match   (:current-route db)
          controllers (apply-controllers (:controllers old-match) new-match)]
      (assoc db :current-route (assoc new-match :controllers controllers)))))

(defn api-request-flow
  "This async-flow handles making an API request when the app-db may not have
  the current authentication data for the user even though the user is logged
  in. For example, when initially loading the application from a non-root path;
  e.g. http://localhost:3428/#/new-game

  The flow makes the request and if a 401 response happens it makes a request to
  get the authentication data (using the `::login` event). Once the
  authentication data has been received (signified by `::login-success`
  occurring), then the original request is made again.

  The second request to the API endpoint does not include checking for another
  401 response. The authentication data should exist by then, so another
  401 response indicates some other issue."
  [{:keys [_method _uri _on-success on-failure] :as api-request-data}]
  {:first-dispatch [::api-request (assoc api-request-data
                                    :on-failure
                                    [::api-request-failure on-failure])]
   :rules          [{:when     :seen?
                     :events   ::api-request-401
                     :dispatch [::login]}
                    {:when     :seen?
                     :events   ::login-success
                     :dispatch [::api-request api-request-data]
                     :halt?    true}
                    ; Either of these failures stop the flow.
                    {:when   :seen?
                     :events ::login-failure
                     :halt?  true}
                    {:when   :seen?
                     :events on-failure
                     :halt?  true}]})

(reg-event-fx
  ::api-request
  ; Make an API request. The api-request-map is expected to have the following
  ; keys: :method, :uri, :on-success, :on-failure. This event adds the
  ; Authorization header and the request/response formats, which are identical
  ; for all API requests.
  (fn [{:keys [db]} [_ api-request-map]]
    (let [jwt (:jwt db)]
      {:http-xhrio (-> api-request-map
                       (update :headers merge {"Authorization" (str "Bearer " jwt)})
                       (assoc
                         :response-format json-response-format
                         :format (ajax/json-request-format)))})))

(reg-event-fx
  ::api-request-failure
  ; Handles an API request failure. This is expected to be used inside an
  ; async-flow.
  ;
  ; If the response status is 401 then dispatch the ::api-request-401 event for
  ; the async-flow to detect. If it is not a 401 then dispatch the given
  ; on-failure-event.
  (fn [_ [_ on-failure-event response]]
    {:fx [[:dispatch (if (= 401 (:status response))
                       [::api-request-401]
                       on-failure-event)]]}))

(reg-event-fx
  ::api-request-401
  ; A no-op event used by the `api-request-flow`.
  (fn [_ _] {}))

(reg-event-db
  ::login-success
  (fn [db [_ {:keys [jwt user]}]]
    (assoc db
      :user user
      :jwt jwt)))

(reg-event-fx
  ::login-failure
  (fn [_ _]
    (println "login-failure!")
    {:set-route {:url "/"}}))

(reg-event-fx
  ::login
  (fn [_ _]
    {:http-xhrio {:method          :get
                  :uri             "/api/login"
                  :response-format json-response-format
                  :on-success      [::login-success]
                  :on-failure      [::login-failure]}}))

(reg-event-fx
  ::logout-success
  (fn [{:keys [db]} _]
    {:set-route {:url "/"}
     :db        (dissoc db :user :jwt)}))

(reg-event-fx
  ::logout-failure
  (fn [_ _]
    (println "logout-failure!")
    {:set-route {:url "/"}}))

(reg-event-fx
  ::logout
  (fn [_ _]
    {:async-flow (api-request-flow
                   {:method     :get
                    :uri        "/api/logout"
                    :on-success [::logout-success]
                    :on-failure [::logout-failure]})}))

(reg-event-db
  ::clear-game
  (fn [db]
    (dissoc db :game)))

(reg-event-fx
  ::store-game
  (fn [{:keys [db]} [_ game-state]]
    {:db (assoc db :game game-state)}))

(reg-event-fx
  ::store-game-and-redirect
  (fn [_ [_ game-state]]
    {:fx        [[:dispatch [::store-game game-state]]]
     :set-route {:url (str "/game/" (:id game-state))}}))

(reg-event-fx
  ::new-game-failure
  (fn [_ _]
    (println "new-game-failure!")
    {:set-route {:url "/"}}))

(reg-event-fx
  ::new-game
  (fn [_ _]
    {:async-flow (api-request-flow
                   {:method     :get
                    :uri        "/api/new-game"
                    :on-success [::store-game-and-redirect]
                    :on-failure [::new-game-failure]})}))

(reg-event-fx
  ::ensure-game-failure
  (fn [_ _]
    (println "ensure-game-failure")
    {:set-route {:url "/"}}))

(reg-event-fx
  ::ensure-game
  (fn [{:keys [db]} [_ game-uuid]]
    (when-not (:game db)
      {:async-flow (api-request-flow
                     {:method     :get
                      :uri        (str "/api/game/" game-uuid)
                      :on-success [::store-game-and-redirect]
                      :on-failure [::ensure-game-failure]})})))

(reg-event-fx
  ::guess-failure
  (fn [_ _]
    (println "guess-failure")))

(reg-event-fx
  ::guess
  (fn [{:keys [db]} [_ letter]]
    (when-not (get-in db [:game :over?])
      (let [game-uuid (get-in db [:game :id])]
        {:async-flow (api-request-flow
                       {:method     :post
                        :uri        (str "/api/game/" game-uuid "/guess")
                        :params     {:letter letter}
                        :on-success [::store-game]
                        :on-failure [::guess-failure]})}))))

(reg-event-db
  ::clear-games
  (fn [db]
    (dissoc db :games)))

(reg-event-db
  ::user-games-success
  (fn [db [_ games]]
    (assoc db :games games)))

(reg-event-fx
  ::user-games-failure
  (fn [_ _]
    (println "user-games-failure")))

(reg-event-fx
  ::user-games
  (fn [{:keys [_db]}]
    {:async-flow (api-request-flow
                   {:method     :get
                    :uri        "/api/games"
                    :on-success [::user-games-success]
                    :on-failure [::user-games-failure]})}))

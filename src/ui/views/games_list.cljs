(ns ui.views.games-list
  (:require
    [clojure.string :as str]
    [reitit.frontend.easy :as rfe]
    [ui.routes :as routes]
    [ui.subs :as subs]
    [ui.utils :refer [<sub]]
    [ui.views.components.hr :as hr]))

(defn- game-item
  [{:keys [over? uuid win? word]}]
  [:div.grid.grid-cols-3.gap-4.border.rounded.my-2
   {:class (cond-> []
             (and over? (not win?)) (conj "border-red-500")
             (and over? win?) (conj "border-emerald-500"))}
   [:div.px-3
    (if over?
      word
      (str/join " " (repeat (count word) "-")))]
   [:div.text-center
    (if over?
      (if win? [:span.font-bold "SUCCESS!"] "💀")
      "In-progress")]
   [:div.text-center
    [:a
     {:href (rfe/href ::routes/game-board {:game-uuid uuid})}
     "View"]]])

(defn- game-list
  [games]
  [:div
   (for [g games]
     ^{:key (:uuid g)}
     [game-item g])])

(defmethod routes/current-route-panel ::routes/past-games
  []
  (let [games (<sub [::subs/games])]
    (if (seq games)
      [game-list games]
      [:div (hr/standard) [:em "No Games Found"]])))

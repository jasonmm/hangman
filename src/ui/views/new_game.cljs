(ns ui.views.new-game
  (:require
    [ui.routes :as routes]
    [ui.subs :as subs]
    [ui.utils :refer [<sub]]))

(defmethod routes/current-route-panel ::routes/new-game
  []
  (if-let [_state (<sub [::subs/game])]
    [:div [:strong "A game exists?? Why?"]]
    [:div [:em "Creating game...."]]))

(ns ui.views.home
  (:require
    [reitit.frontend.easy :as rfe]
    [ui.routes :as routes]
    [ui.subs :as subs]
    [ui.utils :refer [<sub]]
    [ui.views.components.hr :as hr]))

(defn- main-menu-link
  [{:keys [href label]}]
  [:a.bg-white.border.shadow.my-2.p-3.rounded-lg.text-center
   {:href  href
    :class "w-3/4"}
   label])

(defn- main-menu
  []
  [:<>
   [main-menu-link {:href  (rfe/href ::routes/new-game)
                    :label "New Game"}]
   [main-menu-link {:href  (rfe/href ::routes/past-games)
                    :label "Past Games"}]
   [hr/standard]
   [main-menu-link {:href  (rfe/href ::routes/logout)
                    :label "Logout"}]])

(defn- login-menu
  []
  [:ul
   [:li
    [:a {:href "/oauth/github"} "Login With GitHub"]]])

(defmethod routes/current-route-panel ::routes/home
  []
  (if (<sub [::subs/user])
    [main-menu]
    [login-menu]))

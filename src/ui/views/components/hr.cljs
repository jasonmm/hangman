(ns ui.views.components.hr)

(defn standard
  []
  [:hr.h-1.mx-auto.my-4.bg-sky-700.border-0.rounded {:class "w-1/2"}])

(ns ui.views.game-board
  (:require
    [clojure.string :as str]
    [re-frame.core :as re]
    [ui.events :as events]
    [ui.routes :as routes]
    [ui.subs :as subs]
    [ui.utils :refer [<sub] :as u]
    [ui.views.components.hr :as hr]))

(defn mistakes-section
  "Display a section for showing how many mistakes the player has and has made."
  [mistakes allowed-mistakes]
  [:div.text-center
   {:class (cond-> []
             (>= mistakes allowed-mistakes) (conj "text-red-700"))}
   (str "Mistakes: " mistakes " of " allowed-mistakes)])

(defn word-state->string
  ([word-state]
   (word-state->string word-state ""))
  ([word-state separator]
   (str/join separator word-state)))

(defn word-state
  [word-state]
  [:div.text-center.my-2 (word-state->string word-state " ")])

(defn guessed-letter
  [{:keys [word-state]} letter]
  (let [wrong-guess? (not (str/includes? (word-state->string word-state) letter))]
    [:div.col-span-1
     {:class (cond-> []
               wrong-guess? (conj "bg-red-300" "font-bold" "rounded"))}
     letter]))

(defn guessed-letters
  [{:keys [guesses] :as game-state}]
  [:div.border.rounded.p-1
   [:div.font-bold "Guessed Letters"]
   [:div.text-center.p-2
    (into
      [:div.grid.grid-cols-5.gap-1]
      (map (partial guessed-letter game-state) guesses))]])

(defn game-over-success
  []
  [:div.text-center.font-bold "You Won!"])

(defn game-over-failure
  [target-word]
  [:div.text-center
   [:div "You lost!"]
   [:div "The word was " [:span.font-bold target-word]]])

(defn game-over
  [{:keys [over? target-word word-state] :as _game-state}]
  (if (and over? (= target-word (word-state->string word-state)))
    [game-over-success]
    [game-over-failure target-word]))

(defmethod routes/current-route-panel ::routes/game-board
  []
  (if-let [state (<sub [::subs/game])]
    [:div
     {:on-key-press #(re/dispatch [::events/guess (u/which %)])
      :tab-index    1}
     [hr/standard]
     [word-state (:word-state state)]
     [guessed-letters state]
     [mistakes-section (:mistakes state) (get-in state [:settings :max-mistakes])]
     (when (:over? state)
       [:<>
        [hr/standard]
        [game-over state]])]
    [:div [:em "Loading game...."]]))

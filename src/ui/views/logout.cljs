(ns ui.views.logout
  (:require
    [ui.routes :as routes]))

; An empty div for displaying during the logout process.
(defmethod routes/current-route-panel ::routes/logout
  []
  [:div])

(ns ui.views
  (:require
    [reitit.frontend.easy :as rfe]
    [ui.routes :as routes]
    [ui.subs :as subs]
    [ui.utils :refer [<sub]]
    [ui.views.game-board]
    [ui.views.games-list]
    [ui.views.home]
    [ui.views.logout]
    [ui.views.new-game]))

(defn- container
  [body]
  [:div.flex.flex-col.justify-center.items-center.max-w-md.m-6.pb-10.mx-auto body])

(defn- logged-in-user
  [user]
  [:div
   [:em "Logged in as "]
   [:strong (:name user)]])

(defn- title
  []
  [:h1.text-4xl.md:text-5xl.text-sky-300.uppercase.font-mono.drop-shadow-xl
   [:a
    {:href (rfe/href ::routes/home)}
    "Hangman"]])

(defn main-panel
  []
  (let [current-route (<sub [::subs/current-route])]
    [container
     [:<>
      [title]

      (when-let [user (<sub [::subs/user])]
        [logged-in-user user])

      [routes/current-route-panel current-route]]]))


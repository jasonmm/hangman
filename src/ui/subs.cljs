(ns ui.subs
  (:require
    [re-frame.core :refer [reg-sub]]))

(reg-sub ::current-route (fn [db _] (:current-route db)))

(reg-sub ::user (fn [db _] (:user db)))

(reg-sub ::game (fn [db _] (:game db)))

(reg-sub ::games (fn [db _] (:games db)))

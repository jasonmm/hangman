(ns hangman.middleware
  (:require
    [hangman.config :as config]
    [hangman.db :as db]
    [hangman.db.game :as db.game]
    [hangman.db.user :as db.user]))

(defn wrap-config
  [handler]
  (fn [request]
    (handler (assoc request :hangman/config config/configuration))))

(defn wrap-db
  [handler]
  (fn [request]
    (handler (assoc request :hangman/db db/db))))

(defn wrap-word-bank
  [handler]
  (fn [request]
    (handler (assoc request :hangman/word-bank db/word-bank))))

(defn wrap-current-user
  [handler]
  (fn [{:keys [claims hangman/db] :as request}]
    (let [user (db.user/by-email db (:email claims))]
      (handler (assoc request :hangman/current-user user)))))

(defn wrap-game
  [handler]
  (fn [{:keys [hangman/current-user hangman/db path-params] :as request}]
    (let [game (db.game/by-game-uuid
                 db
                 {:game-uuid (:game-uuid path-params)
                  :user-id   (:users/id current-user)})]
      (handler (assoc request :hangman/game game)))))

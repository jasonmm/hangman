(ns hangman.db.user
  (:require
    [clojure.string :as str]
    [honey.sql :as sql]
    [next.jdbc :as jdbc]
    [next.jdbc.sql :as next.sql]))

(defn by-email
  [db email]
  (->> {:from   [:users]
        :where  [:= :users.email email]
        :select [:*]}
       sql/format
       (jdbc/execute! db)
       first))

(defn insert!
  [db user]
  (next.sql/insert! db :users user))

(defn ensure-user!
  "Ensure the provided user data is in the database. Returns the user's ID."
  [db {:keys [email] :as user}]
  (if-let [{:keys [users/id]} (by-email db email)]
    id
    (:GENERATED_KEY (insert! db user))))

(defn- win?
  "Given a game row from the database return whether the player won the game."
  [{:keys [games/word games/state]}]
  (and (< (get state :mistakes)
          (or (get-in state [:settings :max-mistakes])
              (get state :max-mistakes)))
       (= word
          (str/join (get state :word-state)))))

(defn game-row->ui
  [row]
  (-> row
      (assoc :over? (boolean (get-in row [:games/state :over?]))
             :win? (win? row)
             :word (get row :games/word)
             :uuid (get-in row [:games/state :id]))
      (dissoc :games/id :games/state :games/word)))

(defn games
  "Return all games for the given user."
  [db {:users/keys [id]}]
  (->> {:from     [:games]
        :where    [:= :games.userid id]
        :select   [:games.id
                   :games.word
                   :games.state
                   ; This monstrosity is getting the timestamp of the last event
                   ; so the resultset can be ordered by that timestamp. It needs
                   ; to be a string so that conversion to JSON does not break.
                   [[:CAST
                     [:TIMESTAMP
                      [:REPLACE
                       [:JSON_EXTRACT
                        :games.events
                        [:CONCAT
                         "$[" [:- [:JSON_LENGTH :games.events] 1] "].timestamp"]]
                       "\""
                       ""]]
                     :!AS :CHAR]
                    :last-event-ts]]
        :order-by [[:last-event-ts :desc]]}
       sql/format
       (jdbc/execute! db)))

(comment
  (require '[hangman.db])
  (games hangman.db/db {:users/id 3})

  [:raw "JSON_EXTRACT(state, '$.over?')"]

  ;JSON_EXTRACT(`from`,CONCAT("$[",JSON_LENGTH(`from`)-1,"]"))

  {:select   [:*] :from :table
   :order-by [[[:case [:< [:now] :expiry-date]
                :created-date :else :expiry-date]
               :desc]]}

  [[:JSON_VALUE :games.state "$.over?"] :game-over?]
  [[:JSON_EXTRACT :games.state "$.word-state"] :word-state]
  #_())

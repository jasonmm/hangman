(ns hangman.db.game
  (:require
    [cheshire.core :as json]
    [honey.sql :as sql]
    [next.jdbc :as jdbc]
    [next.jdbc.sql :as next.sql]))

(defn jsonify-columns
  "Some columns in the `games` table contain JSON data. This function takes a
  row from the `games` table and returns that row with the JSON columns (if they
  exist) converted to maps instead of JSON strings."
  [row]
  (cond-> row
    (:games/events row)
    (update :games/events #(json/parse-string % true))

    (:games/state row)
    (update :games/state #(json/parse-string % true))))

(defn by-id
  [db id]
  (->> {:from   [:games]
        :where  [:= :games.id id]
        :select [:*]}
       sql/format
       (jdbc/execute! db)
       first
       jsonify-columns))

(defn by-game-uuid
  [db {:keys [game-uuid user-id]}]
  (let [hsql (cond-> {:from   [:games]
                      :where  [:and [:= [:JSON_VALUE :games.state "$.id"] game-uuid]]
                      :select [:*]}
               user-id (update :where conj [:= :games.userid user-id]))]
    (->> hsql
         sql/format
         (jdbc/execute! db)
         first
         jsonify-columns)))

(defn state
  "Fetch the state data from the database for the game with the given `id`, and
  return the state as a map."
  [db id]
  (->> {:from   [:games]
        :where  [:= :games.id id]
        :select [:state]}
       sql/format
       (jdbc/execute! db)
       first
       :games/state
       json/parse-string))

(defn insert!
  [db game-row]
  (next.sql/insert!
    db
    :games
    (cond-> game-row
      (:state game-row)
      (update :state json/generate-string)

      (:events game-row)
      (update :events json/generate-string))))

(defn update!
  [db {:games/keys [id events state]}]
  (next.sql/update!
    db
    :games
    (cond-> {}
      events (assoc :events (json/generate-string events))
      state (assoc :state (json/generate-string state)))
    {:id id}))

(defn successful-update!?
  "Calls `update!` and returns a boolean value of whether the update was
  successful."
  [db game]
  (= 1 (->> game
            (update! db)
            :next.jdbc/update-count)))

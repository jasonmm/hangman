(ns hangman.config
  (:require
    [clojure.edn :as edn]))

(def configuration
  (edn/read-string (slurp "config.edn")))

(defn config
  [& ks]
  (get-in configuration ks))

(def oauth2-options
  {:github
   {:authorize-uri          "https://github.com/login/oauth/authorize"
    :access-token-uri       "https://github.com/login/oauth/access_token"
    :client-id              (config :oauth :github :client-id)
    :client-secret          (config :oauth :github :client-secret)
    :scopes                 ["user:email"]
    :launch-uri             "/oauth/github"
    :redirect-uri           "/oauth/github/callback"
    :landing-uri            "/"
    :state-mismatch-handler (fn [_request]
                              {:status  400
                               :headers {}
                               :body    "My State Mismatch"})}})

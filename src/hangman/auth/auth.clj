(ns hangman.auth.auth)

(defmulti user-data
  "Return a map with keys :name, :email, and :username containing the user's
  data fetched from the identity provider. This multi-method takes one argument,
  a map with keys :identity-provider and :token."
  :identity-provider)

(defmethod user-data :default
  [_]
  nil)


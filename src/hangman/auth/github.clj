(ns hangman.auth.github
  (:require
    [clojure.set :as set]
    [cheshire.core :as json]
    [hangman.auth.auth :as auth]
    [org.httpkit.client :as http]))

(defn- http-get
  "A helper function to make a simple HTTP GET request with the `token`."
  [url token]
  (json/parse-string
    (:body @(http/get url {:headers {"Authorization" (str "Bearer " token)}}))
    true))

(defn- primary-email-address
  "Return the primary email address associated with the `token`."
  [token]
  (->> (http-get "https://api.github.com/user/emails" token)
       (filter :primary)
       first
       :email))

(defmethod auth/user-data :github
  [{:keys [token]}]
  (let [email (primary-email-address token)]
    (-> (http-get "https://api.github.com/user" token)
        (select-keys [:name :login])
        (set/rename-keys {:login :username})
        (assoc :email email))))

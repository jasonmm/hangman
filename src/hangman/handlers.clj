(ns hangman.handlers
  (:require
    [buddy.sign.jwt :as buddy-jwt]
    [hangman.auth.auth :as auth]
    [hangman.auth.github]
    [hangman.db.game :as db.game]
    [hangman.db.user :as db.user]
    [hangman.game :as game]
    [ring.middleware.oauth2 :as oauth2]
    [ring.util.response :as response])
  (:import (java.util Date)))

(defn home
  "Return a response of 'index.html'."
  [_request]
  (some-> (response/resource-response "index.html" {:root "public"})
          (response/content-type "text/html; charset=utf-8")))

(defn login
  "If there is at least one access token then fetch the user's data from the
  identity provider, ensure the user is in the database, create a JWT, and
  return that JWT to the client.

  A session is only used to maintain the OAuth state during the login process.
  Since this handler is the end of the OAuth process (the user has successfully
  logged in) the session is no longer needed. The response deletes the session
  and the 'ring-session' cookie."
  [{:keys [hangman/config hangman/db oauth2/access-tokens]}]
  (let [[identity-provider token-data] (first access-tokens)]
    (if-not identity-provider
      (response/status 401)
      (try
        (let [token (:token token-data)
              user  (auth/user-data {:identity-provider identity-provider
                                     :token             token})
              _     (db.user/ensure-user! db user)
              ;jwt   (buddy-jwt/encrypt user (get-in config [:jwt :secret]))
              jwt   (buddy-jwt/sign user (get-in config [:jwt :secret]))]
          (-> (response/response {:jwt  jwt
                                  :user user})
              (assoc :session nil)
              (assoc-in [:cookies "ring-session"] {:value     ""
                                                   :max-age   0
                                                   :path      "/"
                                                   :http-only true})))
        (catch Exception e
          (-> (response/response e)
              (response/status 500)))))))

(defn logout
  [_request]
  (-> (response/status 204)
      (assoc :session nil)))

; todo: accept settings in request body
(defn new-game
  "Based on the settings in the request body, create a new game for the
  logged-in user, and respond with the new game's data."
  [{:keys [claims hangman/db hangman/word-bank]}]
  (let [user-id   (:users/id (db.user/by-email db (:email claims)))
        game-uuid (random-uuid)
        word      (rand-nth word-bank)
        result    (db.game/insert!
                    db
                    {:userid user-id
                     :word   word
                     :state  (game/initial-state game-uuid (count word))
                     :events [{:event     :game-created
                               :timestamp (Date.)}]})
        game-id   (:GENERATED_KEY result)]
    (response/response (db.game/state db game-id))))

(defn game-state
  [{:keys [hangman/game]}]
  (response/response (:games/state game)))

(defn guess-letter
  "Update the game in the database after applying the guessed letter."
  [{:keys [hangman/db hangman/game params]}]
  (let [guessed-letter   (:letter params)
        previous-guesses (set (get-in game [:games/state :guesses]))
        game-after       (game/after-guess game guessed-letter)]
    (cond
      ; Guessing an already guessed letter is a client error.
      (contains? previous-guesses guessed-letter)
      (response/bad-request {:message (str guessed-letter " has already been guessed.")})

      ; Failure to update the database is a server error.
      (not (db.game/successful-update!? db game-after))
      (-> (response/response {:message "Error updating game."})
          (response/status 500))

      ; Everything is good, respond with the new game state.
      :else
      (response/response (:games/state game-after)))))

(defn user-games
  [{:keys [hangman/current-user hangman/db]}]
  (->> current-user
       (db.user/games db)
       (map (comp db.user/game-row->ui db.game/jsonify-columns))
       response/response))


(ns hangman.db
  (:require
    [clojure.edn :as edn]
    [clojure.java.io :as io]
    [hangman.config :as config]
    [next.jdbc :as jdbc]))

(defonce db (jdbc/get-datasource (config/config :db)))
(defonce word-bank (edn/read-string (slurp (io/resource "wordbank.edn"))))



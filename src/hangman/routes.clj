(ns hangman.routes
  (:require
    [hangman.config :as config]
    [hangman.handlers :as h]
    [hangman.middleware :as middleware]
    [ring.middleware.defaults :as ring-defaults]
    [ring.middleware.json :refer [wrap-json-response
                                  wrap-json-params]]
    [ring.middleware.jwt :refer [wrap-jwt]]
    [ring.middleware.keyword-params :refer [wrap-keyword-params]]
    [ring.middleware.oauth2 :refer [wrap-oauth2]]
    [ring.middleware.params :refer [wrap-params]]
    [ring.util.response]))

(def wrap-defaults-options
  (-> ring-defaults/site-defaults
      ; not wanted for all POST routes, so will be added as needed
      (assoc-in [:security :anti-forgery] false)
      ; recommended by https://github.com/weavejester/ring-oauth2#usage
      (assoc-in [:session :cookie-attrs :same-site] :lax)))

(def routes
  "Routes that return an HTML page or are used for OAuth."
  [["/"
    {:middleware [; wrap-params must appear before wrap-oauth2, see wrap-oauth2 docs
                  [wrap-params]
                  [wrap-oauth2 config/oauth2-options]
                  [ring-defaults/wrap-defaults wrap-defaults-options]]}

    ["" {:get h/home :name ::home}]

    ; OAuth routes. These get replaced by the `wrap-oauth2` middleware, but need
    ; to be defined so that reitit knows they exist. Otherwise, reitit will
    ; create a 404 response.
    ["oauth/github" {:get identity}]
    ["oauth/github/callback" {:get identity}]]])

(def api-middleware
  "Middleware used by all the API routes."
  [[middleware/wrap-config]
   [middleware/wrap-db]
   [wrap-json-params]
   [wrap-keyword-params {:parse-namespaces? true}]
   [wrap-oauth2 config/oauth2-options]
   [ring-defaults/wrap-defaults ring-defaults/api-defaults]
   [wrap-json-response]])

(def api-routes
  "API routes intended to be called by the re-frame frontend."
  [["/login" {:get        (fn [r] (h/login r))
              :name       ::login
              :middleware api-middleware}]

   ; The following routes require a JWT in the Authorization header.
   ["/"
    {:middleware (conj api-middleware
                       [wrap-jwt {:issuers {:no-issuer {:alg    :HS256
                                                        :secret (config/config :jwt :secret)}}}]
                       [middleware/wrap-current-user])}
    ["logout" {:get h/logout :name ::logout}]
    ["games" {:get  (fn [r] (h/user-games r))
              :name ::user-games}]
    ["new-game" {:get        (fn [r] (h/new-game r))
                 :name       ::new-game
                 :middleware [[middleware/wrap-word-bank]]}]
    ["game/:game-uuid" {:get        (fn [r] (h/game-state r))
                        :name       ::game-state
                        :middleware [[middleware/wrap-game]]}]
    ["game/:game-uuid/guess" {:post       (fn [r] (h/guess-letter r))
                              :name       ::guess-letter
                              :middleware [[middleware/wrap-game]]}]]])

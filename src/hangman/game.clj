(ns hangman.game
  (:require [clojure.string :as str])
  (:import (java.util Date)))

(defn initial-state
  "Return the state map for a newly created game."
  [uuid word-len]
  {:id         uuid
   :settings   {:max-mistakes 10}
   :mistakes   0
   :word-state (repeat word-len "_")
   :hints      []
   :guesses    []
   :over?      false})

(defn- indexes-of
  "Return the indexes of `s` that contains the character `c`.

  Requires Java9's `Matcher#results` (https://docs.oracle.com/javase/9/docs/api/java/util/regex/Matcher.html#results--)."
  [^String s ^String c]
  (map #(.start %) (-> c
                       first                                ; only use first char
                       str                                  ; ensure it's a string
                       re-pattern                           ; build regex pattern
                       (re-matcher s)                       ; get java.util.regex.Matcher object
                       .results                             ; Java9 method
                       .iterator                            ; Build CLJ seq
                       iterator-seq)))

(defn- new-word-state
  "Return the word-state vector with the guessed-letter put into the same
  positions it occupies in the string `word`.

  Example, for the word 'bananas', the letter 'n' should return a word-state of
  `[_, _, 'n', _, 'n', _]` (the underscores represent items in the passed-in
  `word-state` that remain untouched)."
  [{:keys [word guessed-letter]} word-state]
  (reduce
    (fn [new-word-state i]
      (assoc new-word-state i guessed-letter))
    word-state
    (indexes-of word guessed-letter)))

(defn- completed-event
  "Return an event map for completing game. The arguments are used to determine
  the result of the game."
  [{:keys [mistakes-remaining guessed-word target-word]}]
  {:event     :complete-game
   :result    (cond
                (not (pos-int? mistakes-remaining)) :out-of-guesses
                (str/includes? guessed-word "_") :resigned
                (not= target-word guessed-word) :wrong-word ; shouldn't happen, right?
                :else :success)
   :timestamp (Date.)})

(defn remaining-mistakes
  [game-state]
  (- (get-in game-state [:settings :max-mistakes])
     (get game-state :mistakes)))

(defn completed
  "Return the `game` row map after completing the game."
  [{:games/keys [state word] :as game}]
  (let [mistakes-left   (remaining-mistakes state)
        guessed-word    (str/join (get state :word-state))
        completed-event (completed-event {:mistakes-remaining mistakes-left
                                          :guessed-word       guessed-word
                                          :target-word        word})]
    (-> game
        (update :games/events conj completed-event)
        (assoc-in [:games/state :over?] true)
        (assoc-in [:games/state :target-word] word))))

(defn game-over?
  "Given a game row map from the database return whether the game is over."
  [{:games/keys [state word] :as _game}]
  (let [guessed-word (str/join (get state :word-state))]
    (or (not (pos-int? (remaining-mistakes state)))
        (not (str/includes? guessed-word "_"))
        (= (str/join guessed-word) word))))

(defn after-guess
  "Given a game row map from the database return that map updated after applying
  the `guessed-letter`."
  [game ^String guessed-letter]
  (let [guess-event   {:event     :guess
                       :letter    guessed-letter
                       :timestamp (Date.)}
        bad-guess?    (not (str/includes? (:games/word game) guessed-letter))

        inc-mistakes  (fn [game]
                        (if bad-guess?
                          (update-in game [:games/state :mistakes] inc)
                          game))
        mark-complete (fn [game]
                        (if (game-over? game)
                          (completed game)
                          game))]
    (-> game
        (update :games/events conj guess-event)
        (update-in [:games/state :guesses] conj guessed-letter)
        (update-in [:games/state :word-state]
                   (partial new-word-state
                            {:word           (:games/word game)
                             :guessed-letter guessed-letter}))
        inc-mistakes
        mark-complete)))

(ns hangman.main
  (:gen-class)
  (:require
    [reitit.coercion.spec]
    [reitit.ring :as ring]
    [ring.adapter.jetty :as jetty]
    [ring.middleware.session :refer [wrap-session]]
    [hangman.routes :as routes]))

(defonce server (atom nil))

(def router-opts
  {:data {:middleware []}})

(def app-router
  (ring/router [["" routes/routes]
                ["/api" routes/api-routes]]
               router-opts))

(def default-handler
  (ring/routes
    (ring/create-resource-handler {:path "/"})
    (ring/redirect-trailing-slash-handler)
    (ring/create-default-handler
      {:not-found (fn [& _args]
                    ;(clojure.pprint/pprint ["not found" args])
                    {:status 404 :body "Not there"})})))

(def app
  (ring/ring-handler
    app-router
    default-handler
    ; For some reason wrap-session needs to be here even though it is included
    ; in `wrap-defaults`.
    {:middleware [wrap-session]}))


(defn start-jetty!
  []
  (reset!
    server
    (jetty/run-jetty #'app
                     {:join?                false
                      :port                 3428
                      :send-server-version? false})))

(defn stop-jetty! []
  (.stop @server)
  (reset! server nil))

(defn- main
  [& _args]
  (start-jetty!))


(comment
  (do
    (stop-jetty!)
    (start-jetty!))

  #_())
